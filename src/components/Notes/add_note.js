import React, { Component } from 'react';
import './note.css';

class AddNote extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            title: '',
            description: ''
        }
    }

    AddNote = (e) => {
        e.preventDefault();

        this.props.clicked(this.state);
        this.setState({title: '', description: ''});
    };

    handleTitleChange = (e) => {
        this.setState({
            title: e.target.value
        });
    };

    handleDescriptionChange = (e) => {
        this.setState({
            description: e.target.value
        });
    };

    render() {

        return (

            <div className="card note">
                <div className="card-body">
                    <h3>Add a new Note</h3>
                    <br/>
                    <input className="card-title" placeholder="Title of Note" value={this.state.title} onChange={this.handleTitleChange}/>
                    <p className="card-text">
                        <input type="text" placeholder="Describe Note" value={this.state.description} onChange={this.handleDescriptionChange}/>
                    </p>
                    <button className="btn btn-primary" onClick={this.AddNote}>
                        Add Note
                    </button>
                </div>
            </div>

        );
    };
}

export default AddNote;