import React, { Component } from 'react';
import './note.css';

class UpdateNote extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            title: props.title,
            description: props.description,
            id: props.id
        }
    }

    UpdateNote = (e) => {
        e.preventDefault();

        this.props.updateclicked(this.state);
        this.setState({title: '', description: '', id : ''});
    };

    handleTitleChange = (e) => {
        this.setState({
            title: e.target.value
        });
    };

    handleDescriptionChange = (e) => {
        this.setState({
            description: e.target.value
        });
    };

    render() {

        return (

            <div className="card note">
                <div className="card-body">
                    <h3>Update Note</h3>
                    <br/>
                    <input className="card-title" placeholder="Title of Note"  value={this.state.title} onChange={this.handleTitleChange}/>
                    <p className="card-text">
                        <input type="text" placeholder="Describe Note" value={this.state.description} onChange={this.handleDescriptionChange}/>
                    </p>
                    <button className="btn btn-primary" onClick={this.UpdateNote}>
                        Update Note
                    </button>
                </div>
            </div>

        );
    };
}

export default UpdateNote;