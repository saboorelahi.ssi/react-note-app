import React from "react";
import './note.css';

const Note = props => {

    return (

        <div className="card note">
            <div className="card-body">
                <h4 className="card-title">{props.title}</h4>
                <p className="card-text">
                    {props.description}
                </p>
                <button className="btn btn-primary" onClick={props.deleteclick}>Delete</button>
                <button className="btn-update btn btn-primary" onClick={props.updateclick}>Update</button>
            </div>
        </div>

    );
};

export default Note;