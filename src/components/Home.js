import React, { Component } from "react";
import Note from "../components/Notes/note";
import AddNote from "../components/Notes/add_note";
import UpdateNote from "../components/Notes/update_note";
import NavBar from "../components/Notes/nav_bar";

class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      notes: [
        {
          id: "1",
          title: "Note 1",
          description: "Note 2"
        },
        {
          id: "2",
          title: "Note 2",
          description: "Its a Note 2"
        },
        {
          id: "3",
          title: "Note 3",
          description: "Its a Note 3"
        }
      ],
      update_note_index: -1       // For updating index when user clicked
    };

    // Other method to add to array
    /*this.state.notes.push({
      id: "4",
      title: "Note 4",
      description: "Its a Note 4"
    })*/
  }

  newNoteHandler = newNote => {
    this.setState({notes: [...this.state.notes, newNote]});       // Improved method - es6

    /*const notes = [...this.state.notes]; // make a copy of notes
    notes.push(newNote);
    this.setState({ notes: notes }); // rerender the notes view*/
  };

  updateNoteValuesHandler = updatedNote => {
    const notes = [...this.state.notes]; // make a copy of notes
    notes[this.state.update_note_index] = updatedNote;
    this.setState({ notes: notes }); // rerender the notes view

    this.setState({update_note_index: -1});     // Hide Update
  };

  deleteNoteHandler = noteIndex => {
    // console.log(noteIndex);

    const notes = [...this.state.notes];
    notes.splice(noteIndex, 1);         // removing Note
    this.setState({ notes });     // Shorthand property

    // this.setState({ notes: notes });
  };

  updateNoteHandler = noteIndex => {
    this.setState({ update_note_index: noteIndex });
  };

  render() {
    const notes = (
      <div>
        {this.state.notes.map((note, index) => {
          return (
            <Note
              title={note.title}
              description={note.description}
              key={note.id}
              deleteclick={() => this.deleteNoteHandler(index)}
              updateclick={() => this.updateNoteHandler(index)}
            />
          );
        })}
      </div>
    );

    let update_note = null;
    let index_to_update = this.state.update_note_index;
    if(index_to_update != -1)     // It means user click on update button
    {
      update_note = <UpdateNote title={this.state.notes[index_to_update].title}
                                description={this.state.notes[index_to_update].description}
                                key={this.state.notes[index_to_update].id}
                                updateclicked={this.updateNoteValuesHandler}/>
    }

    return (
      <div>

        <NavBar/>

        {notes} {/* Display Notes */}

        <AddNote clicked={this.newNoteHandler} />

        {update_note}

      </div>
    );
  }
}

export default Home;
